#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>

struct object {
  double mass, x, y, velx, vely;
  int color, trace;
};

int field[101][101];

void update_velocities(struct object *objs, size_t n) {
  for (size_t i = 0; i < n; ++i) {
    double f, fx = 0, fy = 0, angle;
    for (size_t j = 0; j < n; ++j) {
      if (j == i) {
        continue;
      }
      f = objs[j].mass * objs[i].mass * objs[j].mass / (1000 * (((objs[i].x - objs[j].x) * (objs[i].x - objs[j].x)) + ((objs[i].y - objs[j].y) * (objs[i].y - objs[j].y))));
      angle = atan2(objs[j].y - objs[i].y, objs[j].x - objs[i].x);
      fx += f * cos(angle);
      fy += f * sin(angle);
    }
    objs[i].velx += fx / objs[i].mass;
    objs[i].vely += fy / objs[i].mass;
  }
  for (size_t i = 0; i < n; ++i) {
    if ((int) objs[i].x >= 0 && (int) objs[i].x <= 100 && (int) objs[i].y >= 0 && (int) objs[i].y <= 100) {
      field[(int) objs[i].y][(int) objs[i].x] = objs[i].trace ? ~i : 0;
    }
    objs[i].x += objs[i].velx;
    objs[i].y += objs[i].vely;
    if ((int) objs[i].x >= 0 && (int) objs[i].x <= 100 && (int) objs[i].y >= 0 && (int) objs[i].y <= 100) {
      field[(int) objs[i].y][(int) objs[i].x] = i + 1;
    }
  }
}

void draw(struct object *objs, size_t n, int fromx, int tox, int fromy, int toy) {
  printf("\f\033[0m");
  for (int y = fromy; y <= toy; ++y) {
    for (int x = fromx; x <= tox; ++x) {
      if (field[y][x] > 0) {
        printf("\033[38;5;%dm\u2588\u2588\033[0m", objs[field[y][x] - 1].color);
      } else if (field[y][x] < 0) {
        printf("\033[38;5;%dm. \033[0m", objs[~(field[y][x])].color);
      } else {
        printf("  ");
      }
    }
    printf("\n");
  }
  for (size_t i = 0; i < n; ++i) {
    printf("\033[38;5;%dmObject %zu\033[0m: x=%-7.2f, y=%-7.2f, vx=%-7.2f, vy=%-7.2f\n", objs[i].color, i, objs[i].x, objs[i].y, objs[i].velx, objs[i].vely);
  }
}

int main() {
  struct object *objs = calloc(3, sizeof(struct object));
  struct object *o1 = objs, *o2 = objs + 1, *o3 = objs + 2;
  o1->mass = 1000; o2->mass = 6; o3->mass = 50;
  o1->x = 50; o1->y = 50; o2->x = 70; o2->y = 50; o3->x = 30; o3->y = 50;
  o1->color = 34; o2->color = 208; o3->color = 190;
  o1->vely = 0; o2->vely = -8; o3->vely = 9;
  o1->trace = 1; o2->trace = 1; o3->trace = 1;
  while (42) {
    draw(objs, 3, 0, 100, 0, 100);
    usleep(100000);
    update_velocities(objs, 3);
  }
}
