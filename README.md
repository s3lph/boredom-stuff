# Boredom Stuff

This will be a collection of small things i wrote out of boredom.  The probably
don't serve any purpose, but may be more or less nice to look at or otherwise
interesting.

## Julia Set

Displaying a julia set on stdout using a greyscale gradient from the
xterm-256color palette.  To view the result, one has to zoom out.

<img alt="A julia set with c=-0.8+0.156i at 1000 iterations" width="500" src="/JuliaSet/screenshot.png" />

## Gravity

Simulate gravity between multiple point-like masses.

<img alt="A simulation of gravity, with one heavy central body and two bodies orbiting around it." width="500" src="/Gravity/screenshot.png" />
