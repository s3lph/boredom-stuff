#include <stdio.h>
#include <complex.h>
int main(int argc, char **argv) {
  int max_iter = 1000;
  double complex c = -0.8 + 0.156 * I;
  double minre = -2.0, maxre = 2.0, restep = 0.005, minim = -1.0, maxim = 1.0, imstep = 0.005;
  int w = (maxre - minre)/restep, h = (maxim - minim)/imstep;
  int data[w][h];
  #pragma omp parallel for collapse(2)
  for (int a = 0; a < w; ++a) {
    for (int b = 0; b < h; ++b) {
      double complex z = (minre + a * restep) + (minim + b * imstep) * I;
      int i;
      for (i = 0; i < max_iter && cabs(z) < 2.0; ++i, z = cpow(z, 2.0) + c);
      data[a][b] = i;
    }
  }
  for (int y = 0; y < (maxim-minim)/imstep - 1; y += 2) {
    for (int x = 0; x < (maxre-minre)/restep; ++x) {
      printf("\033[48;5;%dm\033[38;5;%dm\u2584", data[x][y] == max_iter ? 232 : data[x][y] * 22 / max_iter + 233, data[x][y+1] == max_iter ? 232 : data[x][y+1] * 22 / max_iter + 233);
    }
    printf("\033[0m\n");
  }
}
